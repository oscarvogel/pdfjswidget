Widget para encapsular el visualizador de pdf de la fundacion Mozilla

Primero deben bajar y copiar el repo en /protected/extensions/pdfJS

Ejemplo de utilizacion
<?php
	$this->widget('ext.pdfJS.pdfJS', array('archivo'=>Yii::app()->request->baseUrl. '/media/pdf/ResolucionN03-2013.pdf',
			'opcionesHtml'=>array('ancho'=>780),
		));
?>

si no especifican el ancho tomara por defecto 940px.-