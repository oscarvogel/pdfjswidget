<?php
/**
Autor:
   		Jose Oscar Vogel <oscarvogel@gmail.com>
 * 
 * se tomo como base para la creacion del widget desde la siguiente direccion
 * https://bitbucket.org/christiansalazarh/ejemplowidget/overview
 * 
 * ver.php es un ejemplo aportado por Esteban Adrian Perez <dgeaperez@gmail.com>
 */
class pdfJS extends CWidget {

        // los atributos visibles del widget
        //
		
		public $archivo;						// Nombre del archivo pdf a mostrar
        private $_baseUrl;                      // usada internamente para los assets
		/**
		 * @var array que contiene los atributos HTML para el iFrame.
		*/
		public $opcionesHtml = array();
				
        public function init(){
        	if (!isset($this->archivo))
				throw new CException('Debe setear el Nombre del archivo PDF a mostrar.');
			if (!isset($this->opcionesHtml['ancho']))
				$this->opcionesHtml['ancho'] = '940';
			if (!isset($this->opcionesHtml['alto']))
				$this->opcionesHtml['alto'] = '450';
			if (!isset($this->opcionesHtml['nombre']))
				$this->opcionesHtml['nombre'] = 'verpdf';

            parent::init();
        }

        public function run(){
            // este metodo sera ejecutado cuando el widget se inserta

            // preparamos los assets.
            // lo que se hace simplemente es obtener una entrada en el directorio
            // de /tuapp/assets/ (ese directorio te lo da yii con el codigo que 
            // veras dentro del metodo _prepararassets).
            // luego, copiamos a ese directorio todos nuestros scrips o css
            $this->_prepararAssets();

			echo '<iframe width=' . $this->opcionesHtml['ancho'] . '\'
				height=' . $this->opcionesHtml['alto'] . '\'
				name="' . $this->opcionesHtml['nombre'] .'"
				src="' . $this->_baseUrl . '/ver.php?leer=' . $this->archivo . '" >';
			echo '</iframe>';
			
        }// end run()

        
        /*      este metodo tiene como proposito desplegar los assets
                que estan en el directorio privado "maestro" del directorio
                del widget, cuyo destino sera el directorio /assets de tu app.
        */
        public function _prepararAssets(){
            // queremos que el recurso CSS y JS que tenemos en extensions/demowidget/assets/ 
            // pase a copiarse al directorio de assets/ de la aplicacion:
            $localAssetsDir = dirname(__FILE__) . '/assets';
            // baseUrl contendrá el directorio de assets de nuestra app,
            // aparte de copiar todos los archivos alli presentes al nuevo destino.
            $this->_baseUrl = Yii::app()->getAssetManager()->publish($localAssetsDir);
            // requerimos jQuery ?
            // pues solicitemos jQuery a YII, si ya estaba inserto 
            // yii no lo duplicara.
	       	$cs = Yii::app()->getClientScript();
    		$cs->registerCoreScript('jquery');
			
            // Registramos lo que hay en build
            foreach(scandir($localAssetsDir . '/build') as $f){
                $_f = strtolower('/build/' . $f);
                if(strstr($_f,".swp"))
                    continue;
                if(strstr($_f,".js"))
                	$cs->registerScriptFile($this->_baseUrl.$_f);
                if(strstr($_f,".css"))
                    $cs->registerCssFile($this->_baseUrl."/".$_f);
            }

            // Registramos lo que hay en web
			foreach(scandir($localAssetsDir . '/web') as $f){
                $_f = strtolower('/web/' . $f);
                if(strstr($_f,".swp"))
                    continue;
                if(strstr($_f,".js"))
                	$cs->registerScriptFile($this->_baseUrl.$_f);
                if(strstr($_f,".css"))
                    $cs->registerCssFile($this->_baseUrl."/".$_f);
            }
			
            // en este punto deberia haber un directorio de assets/NNNNNN
            // con una copia de todos los archivos dentro de ext/demowidget/assets/
            // y this->_baseUrl tendrá la ruta de ese directorio de assets.
        }

		
} ?>